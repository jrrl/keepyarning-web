import React from 'react';
import CSSModules from 'react-css-modules';
import style from './style.scss';

const About = ({mainImage, partnerDesc, partnerBlurb, crafterImage, crafterDesc, crafterBlurb}) => (
	<div className="container">
		<div className="row">
			<img className="img-responsive" src={mainImage} />
		</div>
		<div className="row">
			<div className="col-md-3"><h2></h2><p><h4>{partnerBlurb}</h4></p></div>
			<div className="col-md-8 col-md-offset-1">
				<h2><strong>Your Partners</strong></h2>
				<p styleName="top-padding">{partnerDesc}</p>
			</div>
		</div>
		<div className="row" styleName="top-padding-2">
			<div className="col-md-5">
				<img className="img-responsive" src={crafterImage} />
			</div>
			<div className="col-md-7">
				<div className="row"><h2><strong>Your KY Crafters</strong></h2></div>
				<div className="row"><p styleName="top-padding">{crafterDesc}</p></div>
				<div className="row"><p className="col-md"><h4>{crafterBlurb}</h4></p></div>
			</div>
		</div>
	</div>
);

export default CSSModules(About, style);