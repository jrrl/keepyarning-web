import React, { Component } from 'react';
import About from './About';

const SAMPLE_PROPS = {
	mainImage: 'https://placehold.it/1170x450',
	crafterImage: 'https://placehold.it/487x450',
	partnerBlurb: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Si quae forte-possumus. Omnia contraria, quos etiam insanos esse vultis. Ut aliquid scire se gaudeant?',
	crafterBlurb: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cur deinde Metrodori liberos commendas? Quo igitur, inquit, modo? Duo Reges: constructio interrete. Si longus, levis. Scrupulum, inquam, abeunti; Non igitur bene.',
	crafterDesc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac tamen, ne cui loco non videatur esse responsum, pauca etiam nunc dicam ad reliquam orationem tuam. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. Quid, si non modo utilitatem tibi nullam afferet, sed iacturae rei familiaris erunt faciendae, labores suscipiendi, adeundum vitae periculum? Duo Reges: constructio interrete. Videmus igitur ut conquiescere ne infantes quidem possint. Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Primum divisit ineleganter; Ita fit beatae vitae domina fortuna, quam Epicurus ait exiguam intervenire sapienti. Aderamus nos quidem adolescentes, sed multi amplissimi viri, quorum nemo censuit plus Fadiae dandum, quam posset ad eam lege Voconia pervenire. Satisne igitur videor vim verborum tenere, an sum etiam nunc vel Graece loqui vel Latine docendus?',
	partnerDesc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac tamen, ne cui loco non videatur esse responsum, pauca etiam nunc dicam ad reliquam orationem tuam. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. Quid, si non modo utilitatem tibi nullam afferet, sed iacturae rei familiaris erunt faciendae, labores suscipiendi, adeundum vitae periculum? Duo Reges: constructio interrete. Videmus igitur ut conquiescere ne infantes quidem possint. Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Primum divisit ineleganter; Ita fit beatae vitae domina fortuna, quam Epicurus ait exiguam intervenire sapienti. Aderamus nos quidem adolescentes, sed multi amplissimi viri, quorum nemo censuit plus Fadiae dandum, quam posset ad eam lege Voconia pervenire. Satisne igitur videor vim verborum tenere, an sum etiam nunc vel Graece loqui vel Latine docendus?'
}

class AboutContainer extends Component {
	constructor() {
		super();
	}

	render() {
		return (
			<About {...SAMPLE_PROPS}/>
		);
	}
}

export default AboutContainer;