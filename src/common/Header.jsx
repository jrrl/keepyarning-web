import React from 'react';
import CSSModules from 'react-css-modules';
import Link from 'react-toolbox/lib/link';
import Button from 'react-toolbox/lib/button';
import style from './style.scss';


const MENU_ITEMS = [
	{link: '#/', label: 'HOME'},
	{link: '#/about', label: 'ABOUT'},
	{link: '#/magazine', label: 'MAGAZINE'},
	{link: '#/press', label: 'PRESS'},
	{link: '#/store', label: 'STORE'},
	{link: '#/contact', label: 'CONTACT'},
];

const MenuItem = ({link, label}) => (
		<div className="col-md-2"><div className="center-block"><Button href={link} label={label} flat mini /></div></div>
);

const Header = () => (
	<header>
		<div className="row"><div styleName="logo"><img src="assets/images/final_logo_1.png" styleName="logo"/></div></div>
		<div className="row"><em styleName="center-text">Cultivating spaces for Yearners</em></div>
		<div className="row">
			{MENU_ITEMS.map((props) => {
				return <MenuItem  {...props} />
			})}
		</div>
	</header>
);

export default CSSModules(Header, style);