import React from 'react';
import CSSModules from 'react-css-modules';
import Avatar from 'react-toolbox/lib/avatar';
import style from './style.scss';

const Footer = () => (
	<footer>
		<div className="row"><div styleName="logo"><img src="assets/images/final_logo_1.png" styleName="logo" /></div></div>
	</footer>
);

export default CSSModules(Footer, style);