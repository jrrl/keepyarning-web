import React from 'react';
import CSSModules from 'react-css-modules';
import Carousel from 'react-slick';
import { Media, Img, ImgExt, Bd } from 'react-media-object';
import Input from 'react-toolbox/lib/input';
import Checkbox from 'react-toolbox/lib/checkbox';
import Button from 'react-toolbox/lib/button';
import Masonry from 'masonry-layout';
import style from './style.scss';

const CAROUSEL_SETTINGS = {
	dots: false,
	arrows: false
};

const Home = () => (
	<div>
		<div styleName="section">
			<Carousel {...CAROUSEL_SETTINGS}>
				<div><img src="https://placehold.it/1140x450" /></div>
				<div><img src="https://placehold.it/1140x450" /></div>
				<div><img src="https://placehold.it/1140x450" /></div>
			</Carousel>
		</div>
		<div className="row" styleName="section">
			<div className="col-md-4 col-md-offset-2">
				<div className="row"><strong styleName="center-text">A little about us!</strong></div>
				<div className="row">
					<Media styleName="media">
						<Img>
							<ImgExt styleName="icon" alt="" src="assets/images/home/ICONS for homepage-01.png"/>
						</Img>
						<Bd>
							We are a group of dreamers, artists, doers, and makers who enjoy creating personal,
							high quality hand-crafted products.
						</Bd>
					</Media>
					<Media styleName="media">
						<Img>
							<ImgExt styleName="icon" alt="" src="assets/images/home/ICONS for homepage-02.png"/>
						</Img>
						<Bd>
							We are champions of inspired living through well thought out pieces that transcend
							beyond decor. Every piece has a story to be told.
						</Bd>
					</Media>
					<Media styleName="media">
						<Img>
							<ImgExt styleName="icon" alt="" src="assets/images/home/ICONS for homepage-03.png"/>
						</Img>
						<Bd>
							Keep Yarning is a lifestyle brand and livelihood project that aims to empower and
							cultivate communities, and promote a meaningful and purposeful household.
						</Bd>
					</Media>
				</div>
			</div>
			<div className="col-md-4">
				<div className="row"><strong>HI THERE, YEARNER!</strong></div>
				<div className="row">
					<div className="row"><Input type="text" name="name" label="Name" /></div>
					<div className="row"><Input type="email" name="email" label="Email" /></div>
					<div className="row">
						<div className="col-md-6"><Input type="tel" name="contact" label="Contact #" /></div>
						<div className="col-md-6"><Input type="text" name="citycountry" label="City / Country" /></div>
					</div>
					<div className="row">
						<div className="col-md-6">
							<div className="row"><Checkbox label="Inquire on a product"/></div>
							<div className="row"><Checkbox label="Get exclusive updates"/></div>
						</div>
						<div className="col-md-6">
							<Button label="Let's Yearn" raised primary />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div className="row" styleName="section">
			<div id="grid">
				<div className="col-md-3 grid-item" styleName="grid-item">
					<a href=""><img className="img-responsive" src="assets/images/home/DECOR.png" /></a>
				</div>
				<div className="col-md-3 grid-item" styleName="grid-item">
					<a href=""><img className="img-responsive" src="assets/images/home/KITCHEN.png" /></a>
				</div>
				<div className="col-md-3 grid-item" styleName="grid-item">
					<a href=""><img className="img-responsive" src="assets/images/home/HOME.png" /></a>
				</div>
				<div className="col-md-3 grid-item" styleName="grid-item">
					<a href=""><img className="img-responsive" src="assets/images/home/PERSONAL.png" /></a>
				</div>
				<div className="col-md-3 grid-item" styleName="grid-item">
					<a href=""><img className="img-responsive" src="assets/images/home/KIDS.png" /></a>
				</div>
				<div className="col-md-3 grid-item" styleName="grid-item">
					<a href=""><img className="img-responsive" src="assets/images/home/COLLECTION.png" /></a>
				</div>
				<div className="col-md-3 grid-item" styleName="grid-item">
					<a href=""><img className="img-responsive" src="assets/images/home/WHAT WE CAN DO FOR YOU-01.png" /></a>
				</div>
			</div>
		</div>
		<div className="row" styleName="section">
			<div className="col-md-8 col-md-offset-2">
				<div className="row">
					<p className="text-center">WHAT YOU CAN DO FOR THEM</p>
				</div>
				<div className="row">
					<div className="col-md-4">
						<p className="text-center">TEST</p>
					</div>
					<div className="col-md-4">
						<p className="text-center">TEST</p>
					</div>
					<div className="col-md-4">
						<p className="text-center">TEST</p>
					</div>
				</div>
				<div className="row">
					<div className="col-md-4">
						<p className="text-center">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed maximus mi sit amet ex euismod efficitur. Donec mi massa, bibendum.
						</p>
					</div>
					<div className="col-md-4">
						<p className="text-center">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed maximus mi sit amet ex euismod efficitur. Donec mi massa, bibendum.
						</p>
					</div>
					<div className="col-md-4">
						<p className="text-center">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed maximus mi sit amet ex euismod efficitur. Donec mi massa, bibendum.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
);


export default CSSModules(Home, style);