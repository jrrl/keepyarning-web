import React from 'react';
import autoBind from 'react-autobind';
import Masonry from 'masonry-layout';
import imagesLoaded from 'imagesloaded';

import Home from './Home';

class HomeContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		const masonry = new Masonry('#grid', {
			itemSelector: '.grid-item'
		});

		imagesLoaded('#grid', () => {
			masonry.layout();
		});
	}

	render() {
		return <Home/>
	}
}

export default HomeContainer;