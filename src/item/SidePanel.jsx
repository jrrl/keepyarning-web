import React from 'react';
import CSSModules from 'react-css-modules';
import {Card, CardTitle, CardText} from 'react-toolbox/lib/card';
import Table from 'react-toolbox/lib/table';

import style from './style.scss';

const tableModel = {
	key: {type: String},
	value: {type: String}
};

const SidePanel = ({details}) => {
	const prodDetails = [
		{key: "Dimensions", value: details.dimensions},
		{key: "Colors", value: details.colors}
	];

	return (
		<div className="col-md-3">
			<Card>
				<CardTitle
					title={details.name}
					subtitle={"Php " + details.price}
				/>
				<CardText>{details.desc}</CardText>
				<CardTitle title="Product Details"/>
				<CardText>
					<Table
						model={tableModel}
						heading={false}
						selectable={false}
						source={prodDetails}
					/>
				</CardText>
			</Card>
		</div>
	);
};

export default CSSModules(SidePanel, style);