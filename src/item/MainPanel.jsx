import React from 'react';
import autoBind from 'react-autobind';
import Carousel from 'react-slick';

class MainPanel extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	onImageClick(e) {
		this.refs.main.slickGoTo(parseInt(e.target.dataset.index));
	}

	render() {
		const main = this.props.images.main;
		const thumbnails = this.props.images.thumbnails;

		const mainSettings = {
			dots: false,
			arrows: false,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1
		};

		const thumbnailSettings = {
			dots: false,
			arrows: false,
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: false
		};

		return (
			<div className="col-md-9">
				<div className="row">
					<Carousel ref='main' {...mainSettings}>
						{main.map((image, index) => {
							return <div key={index}><img src={image.main}/></div>
						})}
					</Carousel>
				</div>
				<div className="row">
					<Carousel {...thumbnailSettings}>
						{thumbnails.map((image, index) => {
							return <div key={index}><img data-index={index} onClick={this.onImageClick} src={image} /></div>
						})}
					</Carousel>
				</div>
			</div>
		);
	}
}

export default MainPanel;