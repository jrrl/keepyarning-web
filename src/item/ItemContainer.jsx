import React from 'react';
import autoBind from 'react-autobind';

import MainPanel from './MainPanel';
import SidePanel from './SidePanel';

const IMAGES_SAMPLE = {
	main: [
		{
			zoom: "https://placehold.it/1200x1200",
			main: "https://placehold.it/805x500"
		},
		{
			zoom: "https://placehold.it/1200x1200",
			main: "https://placehold.it/360x360"
		},
		{
			zoom: "https://placehold.it/1200x1200",
			main: "https://placehold.it/620x500"
		},
		{
			zoom: "https://placehold.it/1200x1200",
			main: "https://placehold.it/600x300"
		},
	],
	thumbnails: [
		"https://placehold.it/120x120",
		"https://placehold.it/120x120",
		"https://placehold.it/120x120",
		"https://placehold.it/120x120"
	]
};

const DETAILS_SAMPLE = {
	name: 'sample',
	price: '10000',
	desc: 'Lorem ipsum blah blah',
	dimensions: '100" x 100"',
	colors: ['red', 'white', 'blue']
};

class ItemContainer extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);

		this.state = {
			details: DETAILS_SAMPLE
		};
	}

	componentDidMount() {
		// TODO : add functionality
	}

	render() {
		return (
			<div>
				<MainPanel images={IMAGES_SAMPLE}/>
				<SidePanel details={this.state.details}/>
			</div>
		);
	}
}

export default ItemContainer;