import React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';

import 'react-toolbox/lib/commons.scss';
import 'bootstrap-loader';

import Header from './common/Header';
import Footer from './common/Footer';
import Home from './home/HomeContainer';
import About from './about/AboutContainer';
import Store from './store/StoreContainer';
import Item from './item/ItemContainer';

const App = (props) => (
	<div className="container">
		<Header/>
		{props.children}
		<Footer/>
	</div>
);

const Routing = () => (
	<Router history={hashHistory}>
		<Route path="/" component={App}>
			<IndexRoute component={Home} />
			<Route path="/about" component={About} />
			<Route path="/store" component={Store} />
			<Route path="/store/:itemCode" component={Item} />
		</Route>
	</Router>
);

render(<Routing/>,  document.getElementById('app'));