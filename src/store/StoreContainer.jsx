import React from 'react';
import autoBind from 'react-autobind';
import Store from './Store';

const CATEGORIES_SAMPLE = {
	name: 'Category',
	items: [
		{name: 'Home', value: 'Home'},
		{name: 'Kitchen', value: 'Home'},
		{name: 'Decor', value: 'Home'},
		{name: 'Pets', value: 'Home'},
		{name: 'Personal', value: 'Home'},
		{name: 'Kids', value: 'Home'}
	]
};

const ITEMS_SAMPLE = [
	{imageUrl: "https://placehold.it/200x200", name: 'Sample 1', price: 100, addInfo: 'Lorem Ipsum', code: "test1"},
	{imageUrl: "https://placehold.it/200x200", name: 'Sample 2', price: 100, addInfo: 'Lorem Ipsum', code: "test2"},
	{imageUrl: "https://placehold.it/200x200", name: 'Sample 3', price: 100, addInfo: 'Lorem Ipsum', code: "test3"},
	{imageUrl: "https://placehold.it/200x200", name: 'Sample 4', price: 100, addInfo: 'Lorem Ipsum', code: "test4"},
	{imageUrl: "https://placehold.it/200x200", name: 'Sample 5', price: 100, addInfo: 'Lorem Ipsum', code: "test5"},
	{imageUrl: "https://placehold.it/200x200", name: 'Sample 6', price: 100, addInfo: 'Lorem Ipsum', code: "test6"},
	{imageUrl: "https://placehold.it/200x200", name: 'Sample 7', price: 100, addInfo: 'Lorem Ipsum', code: "test7"},
];


class StoreContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			categories: [CATEGORIES_SAMPLE],
			items: ITEMS_SAMPLE
		}

		autoBind(this);
	}

	updateFilter(e) {
		e.preventDefault();
		console.log("updateFilter");
		console.log(e);
	}

	render() {
		return (
			<Store categories={this.state.categories} items={this.state.items} onSubmitFilter={this.updateFilter} />
		);
	}
}

export default StoreContainer;