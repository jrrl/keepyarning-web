import React from 'react';
import CSSModules from 'react-css-modules';
import { Link } from 'react-router';
import Button from 'react-toolbox/lib/button';

import style from './style.scss';

const ItemCard = ({imageUrl, code, name, price, addInfo}) => (
	<div className="col-md-3" styleName="item-card">
		<Link to={"/store/" + code}><img src={imageUrl} alt={name}/></Link>
		<div className="caption">
			<Link to={"/store/" + code}><strong>{name}</strong></Link><br/>
			Php {price}<br/>
			<em>{addInfo}</em>
		</div>
	</div>
);

const ItemGrid = ({items}) => (
	<div className="col-md-10">
		{items.map((item, index) => {
			return <ItemCard {...item} key={index}/>
		})}
	</div>
);

const CategoryItem = ({name, value}) => (
	<Link to={value}>
		<Button flat mini label={name}/>
	</Link>
);

const CategoryList = ({name, items}) => (
	<div className="">
		<h6 className=""><strong>{name.toUpperCase()}</strong></h6>
		{items.map((item, index) => {
			return <CategoryItem {...item} key={index}/>
		})}
	</div>
);

const Filter = ({categories, onSubmitFilter}) => (
	<div className="col-md-2" onSubmit={onSubmitFilter}>
		{categories.map((category, index) => {
			return <CategoryList {...category} key={index}/>
		})}
	</div>
);
const Store = ({categories, onSubmitFilter, items}) => (
	<div>
		<Filter categories={categories} onSubmitFilter={onSubmitFilter}/>
		<ItemGrid items={items}/>
	</div>
);

export default CSSModules(Store, style);