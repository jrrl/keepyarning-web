/**
 * Created by Rafael on 8/2/2016.
 */
// var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ProvidePlugin = require('webpack/lib/ProvidePlugin');
var autoprefixer = require('autoprefixer');
var path = require('path');

module.exports = {
	entry: [
		'webpack-dev-server/client?http://0.0.0.0:8080', // WebpackDevServer host and port
		'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
		'./src/App.jsx'
	],
	module: {
		loaders: [
			{
				test: /(\.js|\.jsx)$/,
				exclude: /node_modules/,
				loaders: ['react-hot', 'babel']
			},
			{
				test:  /(\.scss|\.css)$/,
				loader: "style!css?modules!sass!postcss"
				// loader: 'style!css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]!postcss!sass?sourceMap'
				// loader: ExtractTextPlugin.extract('style!css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass')
			},
			{ test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff' },
			{ test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream' },
			{ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file' },
			{ test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml' },
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				loaders: [
					'file?hash=sha512&digest=hex&name=[hash].[ext]',
					'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
				]
			}
		]
	},
	resolve: {
		extensions: ['', '.js', '.jsx', '.css', '.scss', '.json']
	},
	plugins: [
		// new ProvidePlugin({
		// 	jQuery: 'jquery',
		// 	$: 'jquery',
		// 	jquery: 'jquery'
		// })
	],
	sassLoader: {
		includePaths: [
			path.resolve(__dirname, '/node_modules/react-toolbox')
		]
	},
	output: {
		path: __dirname + '/dist',
		publicPath: '/',
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: './dist'
	},
	postcss: [autoprefixer]
}